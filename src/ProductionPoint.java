

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;


public class ProductionPoint extends Thread{
	private int visitingRobot = 0;
	public int matrixSize;
	private int[][] matrix;
	// An array list of ProducerRobot objects.
	ArrayList<ProducerRobot> ProducerRobots;
	// An array list of VaccineDose objects.
	ArrayList<VaccineDose> vaccineDoses;
	private Random rand;
	private Semaphore matrixSem = new Semaphore(1);

	private Semaphore VaccineSem = new Semaphore(1);
	private Semaphore visitingRobotSem = new Semaphore(1);

	public ProductionPoint(int matrixSize, ArrayList<VaccineDose> vaccineDoses, Random rand)
	{
		this.matrixSize = matrixSize;
		this.matrix = new int[matrixSize][matrixSize];
		ProducerRobots = new ArrayList<ProducerRobot>();
		this.vaccineDoses = vaccineDoses;
		this.rand = rand;
	}
	

	public void addVaccine(int index, int nrVaccine)
	{
		vaccineDoses.get(index).addVaccineDoses(nrVaccine);
	}
	
	public int[][] getMatrix()
	{
		return this.matrix;
	}
	
	public int getVisitingRobot()
	{
		return visitingRobot;
	}
	
	/**
	 * This method is called when a transporter robot decides to visit this production point.
	 */
	public void addVisitingRobot()
	{
		// Acquire a permit for the visitingRobotSem so no two witches change the number of visiting witches at the same time.
		try
		{
			visitingRobotSem.acquire();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		if(visitingRobot == 0)
		{
			try
			{
				VaccineSem.acquire();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
		// Tell the production point this transporter robot is visiting.
		this.visitingRobot++;
		
		// Release a permit for the visitingRobot semaphore so other transporter robots can tell the production point they are visiting.
		visitingRobotSem.release();
	}
	
	/**
	 * This method is called when a transporter robot decides to leave this production point.
	 */
	public void takeVisitingRobot()
	{
		try
		{
			visitingRobotSem.acquire();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		

		this.visitingRobot--;
	
		if(visitingRobot == 0)
		{
			VaccineSem.release();
		}
		
		visitingRobotSem.release();
	}
	
	public Semaphore getMatrixSem()
	{
		return matrixSem;
	}
	
	public Semaphore getVaccineSem()
	{
		return VaccineSem;
	}
	
	public void run()
	{

		long initialTime = System.currentTimeMillis();
		
		while(true)
		{	
			// Get the current time.
			long testTime = System.currentTimeMillis();
			
			// If 10 seconds have passed then reset the number of vaccineDoses each demon has to 0 and put them to sleep for 1 second.
			if(testTime >= (initialTime + 10000))
			{
				initialTime = testTime;
				
				for(int it = 0; it < ProducerRobots.size(); ++it)
				{
					ProducerRobots.get(it).sleepDemon();
					
					for(int it2 = 0; it2 < ProducerRobots.get(it).vaccineDoses.size(); ++it2)
					{
						ProducerRobots.get(it).vaccineDoses.get(it2).setNumberVaccineDoses(0);
					}
				}
			}
		}
	}
}