

import java.util.Random;

/**
 * The Pharmacy gets vaccines from the ProducerRobot.Pharmacy doesn't have an array list of Vaccine objects because he
 * doesn't care about the vaccineDoses not about the Vaccine dose names. 
 */
public class Pharmacy extends Thread{
	// The number of vaccines the Pharmacy has.
	private int prodVaccines;
	// Used to generate random numbers.
	private Random rand;
	// Used to get a random number of vaccines,between 2-5
	private int randomProdVaccine;
	
	/**
	 * A parametrized constructor.
	 * 
	 * @param rand Used to generate random numbers.
	 */
	public Pharmacy(Random rand)
	{
		this.rand = rand;
	}
	
	/**
	 * Set the number of vaccines to a specified amount.
	 * 
	 * @param prodPoints The number of vaccines will be set to this value.
	 */
	public void setProdVaccines(int prodPoints)
	{
		this.prodVaccines = prodPoints;
	}
	
	public int getProdPoints()
	{
		return prodVaccines;
	}
	
	/**
	 * Increase the number of vaccines by a specified amount.
	 */
	public void addProdPoints(int prodVaccines)
	{
		this.prodVaccines = this.prodVaccines + prodVaccines;
	}
	
	public void takeProdPoints()
	{
		// Generate a random number.
		randomProdVaccine = rand.nextInt(4) + 2;
		
		// If there are less vaccines than that number.
		if(prodVaccines < randomProdVaccine)
		{
			// If the number of vaccines is at least 2,take them from the farmacy.
			if(prodVaccines >= 2)
			{
				prodVaccines = prodVaccines - 2;
			}
		}
		// Otherwise just take as many vaccines as the random number.
		else
		{
			prodVaccines = prodVaccines - randomProdVaccine;
		}
	}
	
	/**
	 * The run method of the Farmacy.
	 * It just shows how the number of vaccines fluctuates when the program runs.
	 * The number of vaccines might jump to high value because the ProducerRobots have social skills
	 * which makes them create even more vaccines.
	 * The number of vaccines often stays 0 because it takes a while until all the ProducerRobots are spawned. 
	 */
	public void run()
	{
		while(true)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			System.out.println("The Pharmacy has " + prodVaccines + " vaccine dose.");
		}
	}
}
