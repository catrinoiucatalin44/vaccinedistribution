

import java.util.ArrayList;


public class Vaccine {
	private String vaccineName;
	private int numberVaccines;

 	public ArrayList<VaccineDose> vaccineDoses;

	public Vaccine(String vaccineName, ArrayList<VaccineDose> vaccineDoses ) 
	{
		this.vaccineName = vaccineName;
		numberVaccines = 0;
		this.vaccineDoses = vaccineDoses;
	}
	
	public String getVaccineName() 
	{
		return vaccineName;
	}
	
	public int getNrVaccines()
	{
		return vaccineDoses.size();
	}
	
	public int getNumberVaccines()
	{
		return numberVaccines;
	}
	
	/**
	 * Increase the number of vaccines by a specified amount.
	 * 
	 * @param numberVaccines The number of vaccines that will be added to this vaccine dose.
	 */
	public void addVaccines(int numberVaccines)
	{
		this.numberVaccines = this.numberVaccines + numberVaccines;
	}
	
	/**
	 * Decrease the number of vaccines by a specified amount.
	 * 
	 * @param numberVaccines The number of vaccines that will be taken from this vaccine dose.
	 */
	public void takeVaccines(int numberVaccines)
	{
		this.numberVaccines = this.numberVaccines - numberVaccines;
	}
	
	/**
	 * Set the number of vaccines to a specified amount.
	 * 
	 * @param numberVaccines The number of vaccines will be set to this value.
	 */
	public void setNumberVaccines(int numberVaccines)
	{
		this.numberVaccines = numberVaccines;
	}
}