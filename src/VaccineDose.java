
public class VaccineDose {
	private String vaccineName;
	private int numberVaccineDoses;
	
	public VaccineDose(String vaccineName)
	{
		this.vaccineName = vaccineName;
		numberVaccineDoses = 0;
	}
	
	public String getVaccineName()
	{
		return vaccineName;
	}
	
	public int getNumberVaccineDoses()
	{
		return numberVaccineDoses;
	}
	
	public void addVaccineDoses(int numberVaccineDoses)
	{
		this.numberVaccineDoses = this.numberVaccineDoses + numberVaccineDoses;
	}
	
	public void takeVaccineDoses(int numberVaccineDoses)
	{
		this.numberVaccineDoses = this.numberVaccineDoses - numberVaccineDoses;
	}
	
	public void setNumberVaccineDoses(int numberVaccineDoses)
	{
		this.numberVaccineDoses = numberVaccineDoses;
	}
}