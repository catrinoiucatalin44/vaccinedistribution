

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;


public class TransporterRobot extends Thread{
	// These variables are described at the constructor.
	Pharmacy pharmacy;
	ArrayList<ProductionPoint> productionPoints;
	ArrayList<Vaccine> vaccines;
	private Random rand;
	// A semaphore used to prevent the witches to send vaccines to Farmacy at the same time.
	static volatile Semaphore VaccineSem = new Semaphore(1);
	
	/**
	 * A parametrized constructor.
	 * 
	 * @param pharmacy A class object used by the transporter robot to send vaccines to Farmacy.
	 * @param vaccines An array list of Vaccine objects used to store the vaccines and to know their different vaccineDoses.
	 * @param productionPoints An array list of ProductionPoint objects used to access the productionPoints when the transporter robot visit random productionPoints.
	 * @param rand Used to generate random numbers.
	 */
	public TransporterRobot(Pharmacy pharmacy, ArrayList<Vaccine> vaccines, ArrayList<ProductionPoint> productionPoints, Random rand)
	{
		this.pharmacy = pharmacy;
		this.vaccines = vaccines;
		this.productionPoints = productionPoints;
		this.rand = rand;
	}
	
	public void createVaccineRandomProductionPoint()
	{

		int productionPointNumber;
		
		productionPointNumber = rand.nextInt(productionPoints.size());

		productionPoints.get(productionPointNumber).addVisitingRobot();

		
		System.out.println(Thread.currentThread().getName() + " is going " + productionPoints.get(productionPointNumber).getName());
		try
		{
			Thread.sleep(100);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		for(int it1 = 0; it1 < vaccines.size(); ++it1)
		{
			for(int it2 = 0; it2 < vaccines.get(it1).vaccineDoses.size(); ++it2)
			{
				for(int it3 = 0; it3 < productionPoints.get(productionPointNumber).vaccineDoses.size(); ++it3)
				{
					if(vaccines.get(it1).vaccineDoses.get(it2).getVaccineName().equals(productionPoints.get(productionPointNumber).vaccineDoses.get(it3).getVaccineName()))
					{
						if(productionPoints.get(productionPointNumber).vaccineDoses.get(it3).getNumberVaccineDoses() > 15)
						{
							vaccines.get(it1).vaccineDoses.get(it2).addVaccineDoses(15);
							productionPoints.get(productionPointNumber).vaccineDoses.get(it3).takeVaccineDoses(15);
						}
						else
						{
							vaccines.get(it1).vaccineDoses.get(it2).addVaccineDoses(productionPoints.get(productionPointNumber).vaccineDoses.get(it3).getNumberVaccineDoses());
							productionPoints.get(productionPointNumber).vaccineDoses.get(it3).setNumberVaccineDoses(0);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Send vaccines to Farmacy
	 */
	public void sendVaccines()
	{
		// Acquire a permit for the semaphore so no two transportor robot send at the same time.
		try
		{
			VaccineSem.acquire();
		}
		catch (InterruptedException e1)
		{
			e1.printStackTrace();
		}

		for(int it = 0; it < vaccines.size(); ++it)
		{
			if(vaccines.get(it).getNumberVaccines() > 0)
			{	
				pharmacy.addProdPoints(vaccines.get(it).getNumberVaccines());
				
				vaccines.get(it).setNumberVaccines(0);
			}
		}
		
		// Release a permit for the semaphore so other transporter robot can send vaccineDoses.
		VaccineSem.release();
	}
	
	/**
	 * Take vaccines to the Sorcerer by calling one of Sorcerer's methods.
	 */
	public void requestVaccines()
	{
		// Acquire a permit for the semaphore so no transportor robot take at the same time.
		try
		{
			VaccineSem.acquire();
		}
		catch (InterruptedException e1)
		{
			e1.printStackTrace();
		}
		
		pharmacy.takeProdPoints();
		
		// Release a permit for the semaphore so other transportor robot can take vaccineDoses.
		VaccineSem.release();;
	}
	
	/**
	 * The overridden run method.
	 * In an infinite loop it puts the witch to sleep then it tells her to create vaccines and send them to Farmacy.
	 */
	public void run()
	{
		while(true)
		{	
			try
			{
				Thread.sleep(rand.nextInt(100));
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			createVaccineRandomProductionPoint();
			
			sendVaccines();
		}
	}
}