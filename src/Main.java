

import java.util.ArrayList;
import java.util.Random;

/*
 * The Test class contains the main method.
 * Here all the objects of the other classes are created and the threads are starting their executions.
 */
public class Main {

	public static void main(String[] args)
	{
		// Used to generate random numbers.this is also given to all the objects of the classes that generate random numbers.
		Random rand = new Random();
		// Used to get the randomly generated values.
		int tempRand;
		// Used to get the randomly generated values when tempRand already has a value atributed to it.
		int tempRand2;
		// Used to get the randomly generated values.
		int randomVaccineDose;
		// Used to get the randomly generated values.
		int randomNumberVaccineDoses;
		// The Pharmacy object.
		Pharmacy pharmacy;
		// An array list of vaccineDose object that will be given to the class objects that use them.
		ArrayList<VaccineDose> vaccineDoses = new ArrayList<VaccineDose>();
		// An array list of Vaccine object that will be given to the class objects that use them.
		ArrayList<Vaccine> vaccines = new ArrayList<Vaccine>();
		// An array list of ProductionPoint object that will be given to the class objects that use them.
		ArrayList<ProductionPoint> productionPoints = new ArrayList<ProductionPoint>();
		
		// Create each vaccine object and add them into the vaccineDoses array.
		for(int it = 0; it < 10; ++it)
		{
			VaccineDose vaccineDose = new VaccineDose("Vaccine " + it);
			vaccineDoses.add(vaccineDose);
		}
		
		for(int it = 0; it < 20; ++it)
		{
			randomNumberVaccineDoses = rand.nextInt(4) + 4;
			vaccineDoses.clear();
			
			for(int it2 = 0; it2 < randomNumberVaccineDoses; ++it2)
			{
				boolean canAddVaccineDose = true;
				randomVaccineDose = rand.nextInt(10);
				VaccineDose vaccineDose = new VaccineDose("Vaccine " + randomVaccineDose);
				
				for(int it3 = 0; it3 < vaccineDoses.size(); ++it3)
				{
					if(vaccineDoses.get(it3).getVaccineName().equals(vaccineDose.getVaccineName()))
					{
						canAddVaccineDose = false;
					}
				}
				
				if(canAddVaccineDose)
				{
					vaccineDoses.add(vaccineDose);
				}
				else
				{
					do
					{
						canAddVaccineDose = true;
						randomVaccineDose = rand.nextInt(10);
						vaccineDose = new VaccineDose("Vaccine " + randomVaccineDose);
						
						for(int it3 = 0; it3 < vaccineDoses.size(); ++it3)
						{
							if(vaccineDoses.get(it3).getVaccineName().equals(vaccineDose.getVaccineName()))
							{
								canAddVaccineDose = false;
							}
						}
						
						if(canAddVaccineDose)
						{
							vaccineDoses.add(vaccineDose);
						}
					}
					while(canAddVaccineDose == true);
				}
			}
			Vaccine vaccine = new Vaccine("Vaccine " + it, vaccineDoses);
			vaccines.add(vaccine);
		}
		
		// The random number of productionPoints between 2-5
		tempRand = rand.nextInt(3) + 2;
		
		/*
		 *  For each Production Point,generate a random number that represents the size of the matrix,create the production point object,add it
		 *  into the productionPoints array list and start the thread's execution.
		 */
		for(int it = 0; it < tempRand; ++it)
		{
			tempRand2 = rand.nextInt(400) + 100;
			ProductionPoint productionPoint = new ProductionPoint(tempRand2, vaccineDoses, rand);
			productionPoint.setName("Production Point " + it);
			productionPoints.add(productionPoint);
			productionPoint.start();
		}
		
		// Create the Pharmacy object and start the thread's execution.
		pharmacy = new Pharmacy(rand);
		pharmacy.start();
		
		for(int it = 0; it < 10; ++it)
		{
			TransporterRobot transporterRobot = new TransporterRobot(pharmacy, vaccines, productionPoints, rand);
			transporterRobot.setName("Transporter Robot " + it);
			transporterRobot.start();
		}
		
		/*
		 *  In an infinite loop try to spawn a producerRobot in a random productionPointNumber and start that thread's execution.
		 *  If the productionPointNumber is full the producerRobot won't be spawned.
		 */
		while(true)
		{
			tempRand = rand.nextInt(productionPoints.size());
			
			ProducerRobot ProducerRobot = new ProducerRobot(rand, vaccineDoses, productionPoints.get(tempRand));
			
			if(ProducerRobot.spawnProducerRobot(ProducerRobot))
			{
				ProducerRobot.start();
			}
		}
	}

}