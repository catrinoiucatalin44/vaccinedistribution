
import java.util.ArrayList;
import java.util.Random;

/**
 * The ProducerRobot class produces the vaccineDoses that go into the productionPoint.
 */
public class ProducerRobot extends Thread{
	private Random rand;
	/*
	 *  Used so each demon knows it's position in the matrix.
	 *  They are called random because their first value is randomly generated.
	 */
	private int randomLine;
	private int randomColumn;
	// An array list of VaccineDose objects.
	public ArrayList<VaccineDose> vaccineDoses;
	private ProductionPoint productionPoint;
	// When 10 seconds pass in the productionPoint this becomes true and the ProducerRobots sleeps for 1 second.
	private boolean sleepRobotProducer = false;
	private boolean loop = true;
	
	public ProducerRobot(Random rand, ArrayList<VaccineDose> vaccineDoses, ProductionPoint productionPoint)
	{
		this.rand = rand;
		this.vaccineDoses = vaccineDoses;
		this.productionPoint = productionPoint;
	}
	
 boolean spawnProducerRobot(ProducerRobot ProducerRobot)
	{

		if(productionPoint.ProducerRobots.size() < (productionPoint.getMatrix().length / 2))
		{
			productionPoint.ProducerRobots.add(ProducerRobot);
			
			randomLine = rand.nextInt((productionPoint.matrixSize - 2)) + 1;
			randomColumn = rand.nextInt((productionPoint.matrixSize - 2)) + 1;
			
			try
			{
				productionPoint.getMatrixSem().acquire();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			if(productionPoint.getMatrix()[randomLine][randomColumn] != 1)
			{
				productionPoint.getMatrix()[randomLine][randomColumn] = 1;
			}
			else
			{
				do
				{
					randomLine = rand.nextInt(productionPoint.getMatrix().length);
					randomColumn = rand.nextInt(productionPoint.getMatrix().length);
					
					if(productionPoint.getMatrix()[randomLine][randomColumn] != 1)
					{
						productionPoint.getMatrix()[randomLine][randomColumn] = 1;
					}
				}while(productionPoint.getMatrix()[randomLine][randomColumn] == 1);
			}
			
			System.out.println("Create Producer Robot in " + productionPoint.getName());
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			productionPoint.getMatrixSem().release();
			
			return true;
		}
		else
		{
			System.out.println("The maximum number of ProducerRobots in this productionPoint has been reached.");
			return false;
		}
	}
	
	/**
	 * Called by the productionPoint when it has make the ProducerRobots stop for 1 second.
	 */
	public void sleepDemon() {
		sleepRobotProducer = true;
	}
	
	/**
	 * The run method contains a while loop in which the ProducerRobots move and create vaccineDoses.
	 */
	public void run()
	{
		int move;
		int randVaccine;
		int randSleep;
		int wallsHit = 0;
		int stopCreatingVaccines = 0;
		
		while(loop)
		{	
			if(sleepRobotProducer)
			{
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				
				sleepRobotProducer = false;
			}
			
			/*
			 * Get a random number from 1 to 4.
			 * 1 means the producer robot moves left.
			 * 2 means the producer robot moves right.
			 * 3 means the producer robot moves up.
			 * 4 means the producer robot moves down.
			 */
			move = rand.nextInt(4) + 1;
			
			// Acquire a permit for the matrix semaphore so no two ProducerRobots access it at the same time.
			try
			{
				productionPoint.getMatrixSem().acquire();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			if((randomColumn - 1) > 0 && (randomColumn + 1) < (productionPoint.matrixSize - 1) && (randomLine - 1) > 0 && (randomLine - 1) < (productionPoint.matrixSize - 1))
			{
				if((productionPoint.getMatrix()[randomLine][randomColumn - 1] == 1) && (productionPoint.getMatrix()[randomLine][randomColumn + 1] == 1) && (productionPoint.getMatrix()[randomLine + 1][randomColumn] == 1) && (productionPoint.getMatrix()[randomLine - 1][randomColumn] == 1))
				{
					randSleep = rand.nextInt(40) + 10;
					
					try
					{
						Thread.sleep(randSleep);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					
					move = 5;
				}
			}
			
			switch(move)
			{
				case 1:
					if((randomColumn - 1) > 0 && productionPoint.getMatrix()[randomLine][randomColumn - 1] == 0)
					{
						productionPoint.getMatrix()[randomLine][randomColumn] = 0;
						productionPoint.getMatrix()[randomLine][randomColumn - 1] = 1;
						randomColumn--;
						
						if(stopCreatingVaccines == 0)
						{
							try
							{
								productionPoint.getVaccineSem().acquire();
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
							
							
							productionPoint.getVaccineSem().release();
						}
						else
						{
							stopCreatingVaccines--;
						}
						
						try
						{
							Thread.sleep(30);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						if(randomColumn == 0)
						{
							wallsHit++;
							stopCreatingVaccines = 2;
							
						}
						else
						{
							for(int it = 0; it < productionPoint.ProducerRobots.size(); ++it)
							{
								if((productionPoint.ProducerRobots.get(it).randomLine == randomLine) && (productionPoint.ProducerRobots.get(it).randomColumn == randomColumn - 1))
								{
								}
							}
						}
					}
					
					break;
					
				case 2:
					if((randomColumn + 1) < (productionPoint.matrixSize - 1) && productionPoint.getMatrix()[randomLine][randomColumn + 1] == 0)
					{
						productionPoint.getMatrix()[randomLine][randomColumn] = 0;
						productionPoint.getMatrix()[randomLine][randomColumn + 1] = 1;
						randomColumn++;
						
						if(stopCreatingVaccines == 0)
						{
							try
							{
								productionPoint.getVaccineSem().acquire();
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
							
							
							productionPoint.getVaccineSem().release();
						}
						else
						{
							stopCreatingVaccines--;
						}
						
						try
						{
							Thread.sleep(30);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						if(randomColumn == productionPoint.matrixSize - 1)
						{
							wallsHit++;
							stopCreatingVaccines = 2;

						}
						else
						{
							for(int it = 0; it < productionPoint.ProducerRobots.size(); ++it)
							{
								if((productionPoint.ProducerRobots.get(it).randomLine == randomLine) && (productionPoint.ProducerRobots.get(it).randomColumn == randomColumn + 1))
								{
						
								}
							}
						}
					}
					
					break;
				
				case 3:
					if((randomLine - 1) > 0 && productionPoint.getMatrix()[randomLine - 1][randomColumn] == 0)
					{
						productionPoint.getMatrix()[randomLine][randomColumn] = 0;
						productionPoint.getMatrix()[randomLine - 1][randomColumn] = 1;
						randomLine--;
						
						if(stopCreatingVaccines == 0)
						{
							try
							{
								productionPoint.getVaccineSem().acquire();
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
							
							
							productionPoint.getVaccineSem().release();
						}
						else
						{
							stopCreatingVaccines--;
						}
						
						try
						{
							Thread.sleep(30);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						if(randomLine == 0)
						{
							wallsHit++;
							stopCreatingVaccines = 2;
							
						}
						else
						{
							for(int it = 0; it < productionPoint.ProducerRobots.size(); ++it)
							{
								if((productionPoint.ProducerRobots.get(it).randomLine == randomLine - 1) && (productionPoint.ProducerRobots.get(it).randomColumn == randomColumn))
								{
								
								}
							}
						}
					}
					
					break;
					
				case 4:
					if((randomLine + 1) < (productionPoint.matrixSize - 1) && productionPoint.getMatrix()[randomLine + 1][randomColumn] == 0)
					{
						productionPoint.getMatrix()[randomLine][randomColumn] = 0;
						productionPoint.getMatrix()[randomLine + 1][randomColumn] = 1;
						randomLine++;
						
						if(stopCreatingVaccines == 0)
						{
							try
							{
								productionPoint.getVaccineSem().acquire();
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
							
							
							productionPoint.getVaccineSem().release();
						}
						else
						{
							stopCreatingVaccines--;
						}
						
						try
						{
							Thread.sleep(30);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						if(randomLine == productionPoint.matrixSize - 1)
						{
							wallsHit++;
							stopCreatingVaccines = 2;

						}
						else
						{
							for(int it = 0; it < productionPoint.ProducerRobots.size(); ++it)
							{
								if((productionPoint.ProducerRobots.get(it).randomLine == randomLine + 1) && (productionPoint.ProducerRobots.get(it).randomColumn == randomColumn))
								{
								
								}
							}
						}
					}
					
					break;
			}
			
			// Release a permit for the matrix semaphore so other ProducerRobots can access the matrix from the productionPoint.
			productionPoint.getMatrixSem().release();
		}
	}
}